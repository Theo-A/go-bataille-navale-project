package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"math/rand"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"
)

type game struct {
	board       [10][10]string
	coordinate  [10][10]string
	specialShot int
}

func (game *game) createBoard() {
	// Set default board
	for i := 0; i < 10; i++ {
		for j := 0; j < 10; j++ {
			game.board[i][j] = "0"
			game.coordinate[i][j] = "0"
		}
	}

	// Generate ship 4 ship
	for i := 0; i < 4; i++ {
		game.generateShip(i+1, string(rune(65+i)))
	}
}

//return son propre board sans le brouillard de guerre
func (game *game) myBoardHandler(w http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case http.MethodGet:
		var X = [11]string {"  ","A","B","C","D","E","F","G","H","I","J"}
		var Y = [10]string {"1","2","3","4","5","6","7","8","9","10"}
		var board string
		for t := 0; t < len(X); t++ {
			board += X[t]+ " "
		}
		board += "\n"
		for i := 0; i < 10; i++ {
			board += Y[i]+ " "
			if i < 9{
				board += " "
			}
			for j := 0; j < 10; j++ {
				board += game.coordinate[i][j] + " "
			}
			board += "\n"
		}
		fmt.Fprintf(w, "Un 0 correspond à de l'eau et un 1 à un bateau\n"+board)
	}
}

//return le board adverse
func (game *game) boardHandler(w http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case http.MethodGet:
		var board string
		var X = [11]string {"  ","A","B","C","D","E","F","G","H","I","J"}
		var Y = [10]string {"1","2","3","4","5","6","7","8","9","10"}
		for t := 0; t < len(X); t++ {
			board += X[t]+ " "
		}
		board += "\n"
		for i := 0; i < 10; i++ {
			board += Y[i]+ " "
			if i < 9{
				board += " "
			}
			for j := 0; j < 10; j++ {
				board += game.board[i][j] + " "
			}
			board += "\n"
		}
		fmt.Fprintf(w, "Un 0 correspond à une case non touchée et non dévoilée, un 1 à un tir dans l'eau et un 2 à un tir sur un bateau\n"+board)
	}
}

func (game *game) generateShip(length int, letter string) {
	rand.Seed(time.Now().UnixNano())
	x := rand.Intn(9)
	y := rand.Intn(9)
	dir := rand.Intn(3) + 1

	if game.isShipPresent(x, y) || !game.canGenerateShip(x, y, dir, length) {
		game.generateShip(length, letter)
		return
	}

	game.coordinate[x][y] = letter
	for i := 1; i < length; i++ {
		if dir == 1 && y != 0 && !game.isShipPresent(x, y-i) {
			game.coordinate[x][y-i] = letter
		} else if dir == 2 && y != 9 && !game.isShipPresent(x, y+i) {
			game.coordinate[x][y+i] = letter
		} else if dir == 3 && x != 0 && !game.isShipPresent(x-i, y) {
			game.coordinate[x-i][y] = letter
		} else if dir == 4 && x != 9 && !game.isShipPresent(x+i, y) {
			game.coordinate[x+i][y] = letter
		}
	}
}

func (game *game) canGenerateShip(x int, y int, dir int, length int) bool {
	canGenerate := true
	for i := 1; i < length; i++ {
		if dir == 1 && ((y-length < 0) || game.isShipPresent(x, y-i)) {
			canGenerate = false
		} else if dir == 2 && ((y+length > 9) || game.isShipPresent(x, y+i)) {
			canGenerate = false
		} else if dir == 3 && ((x+length < 0) || game.isShipPresent(x-i, y)) {
			canGenerate = false
		} else if dir == 4 && ((x+length > 9) || game.isShipPresent(x, y+i)) {
			canGenerate = false
		}
	}
	return canGenerate
}

func (game *game) isShipPresent(x int, y int) bool {
	if x > 9 || y > 9 || x < 0 || y < 0 {
		return true
	}
	return game.coordinate[x][y] == "A" || game.coordinate[x][y] == "B" || game.coordinate[x][y] == "C" || game.coordinate[x][y] == "D"
}

func server() string {
	var portUser string
	fmt.Println("Sur quelle adresse voulez vous jouez ?")
	fmt.Scanln(&portUser)
	return portUser
}

func (game *game) hitHandler(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodPost {
		fmt.Fprintln(w, "Method is not allowed.")
		return
	}
	if err := req.ParseForm(); err != nil {
		fmt.Fprintln(w, "Something went bad with the form")
		return
	}
	x, errX := strconv.Atoi(req.FormValue("x"))
	y, errY := strconv.Atoi(req.FormValue("y"))
	if errX != nil || errY != nil {
		fmt.Fprintln(w, "Something went bad with coordinate")
		return
	}

	// y = (y - 9) * (-1)
	isShip := game.isShipPresent(y, x)
	if isShip {
		game.board[y][x] = "2"
		game.coordinate[y][x] = "X"
		io.WriteString(w, "Touché !\n")
	} else {
		game.board[y][x] = "1"
		io.WriteString(w, "Raté !\n")
	}
}

func (game *game) fire() string {
	x := map[string]string{"a": "0", "b": "1", "c": "2", "d": "3", "e": "4", "f": "5", "g": "6", "h": "7", "i": "8", "j": "9"}
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Println("Quel est votre cible?")

	//port du joueur cible
	fmt.Println("Joueur : ")
	scanner.Scan()
	targetPlayer := scanner.Text()
	target := "http://localhost:" + targetPlayer + "/hit"

	specialFire := false
	if game.specialShot > 0 {
		fmt.Printf("Voulez vous faire un tir spécial ? (Nombre de tir spécial restant %d) [y/n]: ", game.specialShot)
		scanner.Scan()
		specialFire = strings.ToLower(scanner.Text()) == "y"
	}

	//numero de la case cible
	fmt.Println("Case (A1 - J10) : ")
	scanner.Scan()
	targetCase := scanner.Text()
	cmd := exec.Command("clear") //Linux example, its tested
	cmd.Stdout = os.Stdout
	cmd.Run()

	if value, ok := x[strings.ToLower(string(targetCase[0]))]; ok {

		y := strings.Replace(targetCase, string(targetCase[0]), "", -1)
		number, err := strconv.Atoi(y)
		if err != nil || number > 10 || number < 1 {
			fmt.Println("Merci de rentrer un nombre valide entre 1 et 10")
			game.fire()
		}
		number = number - 1
		y = strconv.Itoa(number)
		var str string
		if specialFire {
			valueInt, err2 := strconv.Atoi(value)
			if err2 != nil {
				fmt.Print("Something went wrong.")
				game.fire()
			}
			str = game.specialFire(valueInt, number, target)
		} else {
			str = shot(value, y, target)
		}
		fmt.Println(str)
	} else {
		fmt.Println("Case non trouvé")
		game.fire()
	}
	return targetPlayer
}

func loose(game game) bool {
	var touche, toucheA, toucheB, toucheC, toucheD int
	loose := false
	touche = 0
	toucheA = 0
	toucheB = 0
	toucheC = 0
	toucheD = 0
	for i := 0; i < 10; i++ {
		for j := 0; j < 10; j++ {
			if game.coordinate[i][j] != "A" {
				toucheA++
				if toucheA == 100 {
					touche++
				}
			}
			if game.coordinate[i][j] != "B" {
				toucheB++
				if toucheB == 100 {
					touche++
				}
			}
			if game.coordinate[i][j] != "C" {
				toucheC++
				if toucheC == 100 {
					touche++
				}
			}
			if game.coordinate[i][j] != "D" {
				toucheD++
				if toucheD == 100 {
					touche++
				}
			}
		}
	}
	fmt.Printf("Vous avez %d bateaux coulés \n", touche)
	if touche == 4 {
		loose = true
	}
	return loose
}

func (game game) resultHandler(w http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case http.MethodGet:
		fmt.Fprintf(w, "La partie n'est pas finie")
		if loose(game) {
			fmt.Fprintf(w, "Vous avez perdu")
		}
	}
}

func (game *game) specialFire(x int, y int, target string) string {
	var str string
	str += shot(strconv.Itoa(x), strconv.Itoa(y), target)
	if x-1 >= 0 && y+1 <= 9 {
		str += shot(strconv.Itoa(x-1), strconv.Itoa(y+1), target)
	}
	if x+1 <= 9 && y+1 <= 9 {
		str += shot(strconv.Itoa(x+1), strconv.Itoa(y+1), target)
	}
	if x+1 <= 9 && y-1 >= 0 {
		str += shot(strconv.Itoa(x+1), strconv.Itoa(y-1), target)
	}
	if x-1 >= 0 && y-1 >= 0 {
		str += shot(strconv.Itoa(x-1), strconv.Itoa(y-1), target)
	}
	game.specialShot--
	return str
}

func shot(x string, y string, target string) string {
	resp, err := http.PostForm(target, url.Values{"x": {x}, "y": {y}})
	if err != nil {
		panic(err)
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	return string(body[:]) + "\n"
}

func main() {
	var game game
	game.specialShot = 3
	end := false
	game.createBoard()

	user := server()
	fmt.Printf("mon adresse est %s\n", user)
	go http.ListenAndServe(":"+user, nil)
	http.HandleFunc("/myboard", game.myBoardHandler)
	http.HandleFunc("/board", game.boardHandler)
	http.HandleFunc("/hit", game.hitHandler)
	http.HandleFunc("/result", game.resultHandler)

	for !end {
		if loose(game) {
			println("vous avez perdu")
			end = true
		} else {
			game.fire()
		}
	}
}
